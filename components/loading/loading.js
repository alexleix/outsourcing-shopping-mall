Component({
  properties: {
    color:{
      type:String,
      value:'#c9c9c9',
    },
    size: {
      type: Number,
      optionalTypes: [String],
      value: 30
    },
    textSize: {
      type: Number,
      optionalTypes: [String],
      value: 14
    }
  },
  data: {
    array12: Array(12)
  },
  methods: {

  },
  created: function(){

  },
  attached: function(){

  },
  ready: function(){

  },
  moved: function(){

  },
  detached: function(){

  },
});
