//Component Object
Component({
  properties: {
    position: {
      type: String,
      value: 'sticky'
    },
    showCircleBackBtn: {
      type: Boolean,
      value: false
    },
    showCommonBackBtn: {
      type: Boolean,
      value: false
    },
    backgroundColor: {
      type: String,
      value: "#FFFFFF"
    },
    title: {
      type: String,
      value: ""
    },
    backBtnBgColor: {
      type: String,
      value: "rgba(0, 0, 0, .2)"
    },
    backBtnColor: {
      type: String,
      value: "#fff"
    }
  },
  data: {
    paddingTop: 10,
    height: 0,
    pageQueue: 0,
    hasPrevPage: true
  },
  methods: {
    back() {
      if (this.data.pageQueue < 2) {
        wx.switchTab({ url: '/pages/home/home' })
      } else {
        wx.navigateBack({ delta: 1 })
      }
    }
  },
  created: function () {
    const {top, bottom} = wx.getMenuButtonBoundingClientRect()
    wx.getSystemInfo({
      success: ({statusBarHeight})=>{
        this.data.height = bottom + (top - statusBarHeight)
        this.data.paddingTop = statusBarHeight
        wx.nextTick(()=>{
          this.setData({
            height: this.data.height,
            paddingTop: this.data.paddingTop
          })
        })
      }
    })
  },
  ready: function () {
    const pages = getCurrentPages()
    // console.log(pages)
    this.setData({ pageQueue: pages.length, hasPrevPage: pages.length !== 1 })
  }
})
