// components/course-item/course-item.js
Component({
  /**
   * 组件的属性列表
   */
  properties: {
    data: {
      type: Object,
      value: {}
    },
    bottomLayout: {
      type: String,
      value: 'flex-start'
    }
  },
  methods: {
    goCourseDetail() {
      wx.navigateTo({ url: `/pages/course-detail/course-detail?id=${this.data.data.id}` })
    }
  }
})
