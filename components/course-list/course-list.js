// components/course-list/course-list.js
Component({
  /**
   * 组件的属性列表
   */
  properties: {
    data: {
      type: Array,
      value: []
    },
    bottomLayout: {
      type: String,
      value: 'flex-start'
    },
    empty: {
      type: Boolean,
      value: false
    },
    emptyText: {
      type: String,
      value: '暂无相关内容'
    }
  }
})
