// components/activity-list/activity-list.js
Component({
  /**
   * 组件的属性列表
   */
  properties: {
    data: {
      type: Object,
      value: {}
    }
  },
  methods: {
    goDetail({ currentTarget }) {
      // console.log(currentTarget.dataset.id)
      const { id, title } = currentTarget.dataset
      wx.navigateTo({ url: `/pages/activity/activity?id=${id}&title=${title}` })
    }
  }
})
