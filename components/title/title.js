//Component Object
Component({
  properties: {
    margin: {
      type:Number,
      value: 18
    },
    title: {
      type: String,
      value: ''
    },
    sub: {
      type: String,
      value: ''
    }
  },
  data: {

  },
  methods: {

  },
  created: function(){

  },
  attached: function(){

  },
  ready: function(){

  },
  moved: function(){

  },
  detached: function(){

  },
});
