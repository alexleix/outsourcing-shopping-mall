//Component Object
Component({
  properties: {
    sortLabels: {
      type: Array,
      value: [
        { label: '综合', sort: false },
        { label: '销量', sort: true, status: 0 },
        { label: '新品', sort: true, status: 0 },
        { label: '价格', sort: true, status: 0 }
      ]
    },
    activeIndex:{
      type:Number,
      value: 0
    }
  },
  data: {},
  observers: {
    activeIndex: (index) => {
    }
  },
  methods: {
    changeActiveIndex({ currentTarget }) {
      const index = currentTarget.dataset.index
      if (this.data.activeIndex === index) {
        if (this.data.sortLabels[index].sort) {
          this.triggerEvent('change-sort-status', { index: index })
        }
      } else {
        this.triggerEvent('change-active-index', { index: index })
      }
    }
  },
  created: function(){

  },
  attached: function(){

  },
  ready: function(){

  },
  moved: function(){

  },
  detached: function(){

  },
});
