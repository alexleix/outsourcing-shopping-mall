//Component Object
Component({
  properties: {
    data:{
      type: Array,
      value: []
    },
    columnGap: {
      type: Number,
      value: 15
    },
    empty: {
      type: Boolean,
      value: false
    },
    emptyText: {
      type: String,
      value: '暂无相关商品'
    }
  }
})
