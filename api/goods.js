const wxRequest = require('../utils/request').wxRequest()

module.exports = {
  wxRequest,
  category: ({ data, callback }) => wxRequest.get({ url: '/api/Category/index', data, callback }),
  goodsList: ({ data, callback }) => wxRequest.post({ url: '/api/Goods/lists', data, callback }),
  goodsDetail: ({ data, callback }) => wxRequest.get({ url: '/api/Goods/detail', data, callback }),
  addFavourite: ({ data, callback }) => wxRequest.post({ url: '/api/MyLikes/addLike', data, callback }),
  cancelFavourite: ({ data, callback }) => wxRequest.post({ url: '/api/MyLikes/delLike', data, callback }),
  addCart: ({ data, header, callback }) => wxRequest.post({ url: '/api/cart/add', data, header, callback }),
  bannerList: ({ data, callback }) => wxRequest.get({ url: '/api/Goods/scroller', data, callback }),
  evaluateList: ({ data }) => wxRequest.get({ url: '/api/Comment/lists', data })
}
