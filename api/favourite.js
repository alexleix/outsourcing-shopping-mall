const wxRequest = require('../utils/request').wxRequest()

module.exports = {
  wxRequest,
  goodsList: ({ data }) => wxRequest.get({ url: '/api/MyLikes/lists', data }),
  courseList: ({ data }) => wxRequest.get({ url: '/api/MyLikes/courseList', data })
}
