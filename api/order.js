const wxRequest = require('../utils/request').wxRequest()

module.exports = {
  wxRequest,
  courseOrder: ({ data, callback }) => wxRequest.get({ url: '/api/UserInfo/getCourses', data, callback }),
  goodsOrder: ({ data, callback }) => wxRequest.get({ url: '/api/UserOrder/lists', data, callback }),
  orderDetail: ({ data, callback }) => wxRequest.get({ url: '/api/UserOrder/detail', data, callback }),
  cancelOrder: ({ data, callback }) => wxRequest.get({ url: '/api/UserOrder/cancel', data, callback }),
  orderPay: ({ data, callback }) => wxRequest.post({
    url: '/api/UserOrder/pay',
    data,
    header: { 'Content-Type': 'application/x-www-form-urlencoded' },
    callback
  }),
  orderGoods: ({ data, callback }) => wxRequest.get({ url: '/api/Comment/comments', data, callback }),
  submitEvaluate: ({ data, callback }) => wxRequest.post({
    url: '/api/Comment/comments',
    data,
    callback,
    header: { 'Content-Type': 'application/x-www-form-urlencoded' }
  }),
  receipt: ({ data, callback }) => wxRequest.get({ url: '/api/UserOrder/receipt', data, callback })
}
