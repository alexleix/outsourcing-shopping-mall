const wxRequest = require('../utils/request').wxRequest()

module.exports = {
  activityDetail: ({ data, callback }) => wxRequest.post({ url: '/api/Activity/details', data, callback }),
  activityList: ({ data, callback }) => wxRequest.get({ url: '/api/Activity/list', data, callback }),
  giveLike: ({ data, callback }) => wxRequest.post({ url: '/api/Activity/addLike', data, callback }),
  disLike: ({ data, callback }) => wxRequest.post({ url: '/api/Activity/delLike', data, callback })
}
