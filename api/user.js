const wxRequest = require('../utils/request').wxRequest()

module.exports = {
  bindUserInfo: ({ data, callback }) => wxRequest.post({ url: '/api/UserInfo/setInfo', data, callback }),
  credit: () => wxRequest.get({ url: '/api/UserInfo/index' }),
  getCreditWithShare: () => wxRequest.get({ url: '/api/Index/shareScore' })
}
