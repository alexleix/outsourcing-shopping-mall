const wxRequest = require('../utils/request').wxRequest()

module.exports = {
  wxRequest,
  cartList: ({ data, callback }) => wxRequest.get({ url: '/api/Cart/lists', data, callback }),
  deleteCart: ({ data, callback }) => wxRequest.post({ url: '/api/Cart/delete', data, callback }),
  decreaseCart: ({ data, callback }) => wxRequest.post({ url: '/api/cart/sub', data, callback }),
  increaseCart: ({ data, callback }) => wxRequest.post({ url: '/api/cart/add', data, callback })
}
