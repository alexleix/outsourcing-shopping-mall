const wxRequest = require('../utils/request').wxRequest()

module.exports = {
  activityListApi: ({ data, callback }) => wxRequest.get({ url: '/api/Activity/list', data, callback })
}
