const wxRequest = require('../utils/request').wxRequest()

module.exports = {
  wxRequest,
  addressList: ({ callback }) => wxRequest.get({ url: '/api/address/list', callback }),
  saveAddress: ({ data, callback, header }) => wxRequest.post({ url: '/api/address/add', data, header, callback }),
  deleteAddress: ({ data, callback }) => wxRequest.post({ url: '/api/address/delete', data, callback }),
  setDefault: ({ data, callback }) => wxRequest.post({ url: '/api/Address/setDefault', data, callback })
}
