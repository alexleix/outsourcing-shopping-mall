const wxRequest = require('../utils/request').wxRequest()

module.exports = {
  wxRequest,
  getCredit: ({ data, callback }) => wxRequest.get({ url: '/api/Order/buyNowIntegral', data, callback }),
  buyNow: ({ data, callback }) => wxRequest.get({ url: '/api/order/buyNow', data, callback }),
  getCart: ({ data, callback }) => wxRequest.get({ url: '/api/order/cart', data, callback }),
  creditInfo: () => wxRequest.get({ url: '/api/UserInfo/getScore' }),
  submitForCredit: ({ data, callback }) => wxRequest.post({
    url: '/api/Order/buyNowIntegral',
    header: { 'Content-Type': 'application/x-www-form-urlencoded' },
    data, callback
  }),
  submitFormGoods: ({ data, callback }) => wxRequest.post({
    url: '/api/order/buyNow',
    header: { 'Content-Type': 'application/x-www-form-urlencoded' },
    data, callback
  }),
  submitFormCart: ({ data, callback }) => wxRequest.post({
    url: '/api/order/cart',
    header: { 'Content-Type': 'application/x-www-form-urlencoded' },
    data, callback
  })
}
