const wxRequest = require('../utils/request').wxRequest()

module.exports = {
  wxRequest,
  category: ({ callback }) => wxRequest.get({ url: '/api/CateCourse/list', callback }),
  courseList: ({ data, callback }) => wxRequest.post({ url: '/api/Courses/list', data, callback }),
  courseDetail: ({ data, callback }) => wxRequest.post({ url: '/api/Courses/detail', data, callback }),
  classList: ({ data, callback }) => wxRequest.get({ url: '/api/Lessons/list', data, callback }),
  myCourse: () => wxRequest.get({ url: '/api/UserInfo/getCourses' }),
  classDetail: ({ data, callback }) => wxRequest.post({ url: '/api/Lessons/detail', data, callback }),
  buyCourse: ({ data, callback }) => wxRequest.post({ url: '/api/order/buyNowCourse', data, callback }),
  getCreditFormWatchVideo: ({ data }) => wxRequest.post({ url: '/api/Courses/addScore', data })
}
