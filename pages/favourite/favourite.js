const { API_OK } = require('../../utils/setting')
const { wxRequest, goodsList, courseList } = require('../../api/favourite')
const app = getApp()

Page({
  data: {
    loading: false,
    type: 'goods',
    list: []
  },
  pageData: {
    page: 1,
    canLoadMore: true
  },
  onLoad: function () {
    this.setData({ loading: true })
    if (wx.getStorageSync('token')) {
      this.getFavourite()
    } else {
      app.tokenReadyCallback = this.getFavourite
      app.getTokenFailedCallback = () => {
        this.setData({ loading: false })
      }
    }
  },
  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
    if (!this.pageData.canLoadMore) return
    this.pageData.page += 1
    this.setData({ loading: true })
    this.getFavourite()
  },
  /**
   * 切换收藏列表
   */
  changeType({ detail }) {
    // console.log(e)
    this.pageData.page = 1
    this.pageData.canLoadMore = true
    this.setData({ type: detail.name, loading: true, list: [] })
    Object.keys(wxRequest.queue).forEach(task => {
      wxRequest.queue[task].abort()
    })
    // console.log('token: ', wx.getStorageSync('token'))
    if (wx.getStorageSync('token')) {
      this.getFavourite()
    } else {
      app.tokenReadyCallback = this.getFavourite
      app.getTokenFailedCallback = () => {
        this.setData({ loading: false })
      }
    }
  },
  /**
   * 获取收藏的列表
   */
  getFavourite() {
    if (this.data.type === 'goods') {
      return this.favouriteGoodsList()
    } else {
      return this.favouriteCourseList()
    }
  },
  /**
   * 获取商品列表
   */
  async favouriteGoodsList() {
    try {
      const res = await goodsList({
        data: { page: this.pageData.page }
      })
      console.log('goods', res)
      if (res.code === API_OK) {
        this.pageData.canLoadMore = res.data.current_page < res.data.last_page
        this.setData({
          loading: false,
          list: res.data.data.map(goods => {
            console.log(goods)
            return {
              id: goods.goods_id,
              image: (goods.goods.image && goods.goods.image.length) ? goods.goods.image[0].file_path : '',
              name: goods.goods.goods_name,
              price: goods.goods_min_price || '0.00'
            }
          })
        })
      }
    } catch(err) {
      console.warn(err)
      this.setData({ loading: false })
    }
  },
  /**
   * 获取课程列表
   */
  async favouriteCourseList() {
    try {
      const res = await courseList({
        data: { page: this.pageData.page }
      })
      console.log('course', res)
      if (res.code === API_OK) {
        this.pageData.canLoadMore = res.data.current_page < res.data.last_page
        this.setData({
          loading: false,
          list: res.data.data.map(item => {
            return {
              id: item.courses_id,
              image: (item.image && item.image.length) && item.image[0].file_path,
              courseName: item.courses.course_name,
              classHour: item.lessons_count,
              purchasedNum: item.courses.sold_count,
              coursePrice: item.courses.course_price
            }
          })
        })
      }
    } catch(err) {
      console.warn(err)
      this.setData({ loading: false })
    }
  }
})
