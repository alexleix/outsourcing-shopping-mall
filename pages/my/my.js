const { credit } = require('../../api/user')
const { API_OK } = require('../../utils/setting')
const app = getApp()

Page({
  data: {
    credit: 0
  },
  onShow: function () {
    if (wx.getStorageSync('token')) {
      this.getCredit()
    } else {
      app.tokenReadyCallback = this.getCredit
    }
  },
  async getCredit() {
    try {
      const res = await credit()
      console.log(res)
      if (res.code === API_OK) {
        this.setData({ credit: res.data.score * 1 })
      }
    } catch(err) {
      console.log(err)
    }
  }
})
