const wxRequest = require('../../utils/request').wxRequest()
const { API_OK } = require('../../utils/setting')

Page({
  data: {
    email: '',
    phone: '',
    address: ''
  },
  onLoad: function (options) {
    this.getConfig()
  },
  phoneCall() {
    wx.makePhoneCall({ phoneNumber: this.data.phone })
  },
  copy({ currentTarget }) {
    wx.setClipboardData({ data: currentTarget.dataset.text })
  },
  async getConfig() {
    wx.showLoading({ title: '拼命加载中', mask: true })
    try {
      const res = await wxRequest.get({ url: '/api/index/common' })
      // console.log(res)
      if (res.code === API_OK) {
        this.setData({
          email: res.data.email,
          phone: res.data.phone,
          address: res.data.address
        })
      }
    } catch(err) {
      console.warn(err)
    } finally {
      wx.hideLoading()
    }
  }
})
