const wxRequest = require('../../utils/request').wxRequest()
const { API_OK } = require('../../utils/setting')

Page({
  data: {
    image: ''
  },
  pageData: {
    serialNumber: 1
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.pageData.serialNumber = +options.number
    this.getData()
  },
  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {},
  async getData() {
    wx.showLoading({ title: '拼命加载中' })
    try {
      const res = await wxRequest.get({ url: '/api/index/common', callback: wx.hideLoading})
      console.log(res)
      if (res.code === API_OK) {
        this.setData({ image: this.pageData.serialNumber === 1 ? res.data.introduce_img : res.data.introduce_img2 })
      }
    } catch(err) {
      console.warn(err)
    }
  }
})
