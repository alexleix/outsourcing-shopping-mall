const { API_OK } = require('../../utils/setting')
const wxRequest = require('../../utils/request').wxRequest()

const app = getApp()

Page({
  data: {
    banner: []
  },
  onLoad() {},
  onShow() {
    this.getHomeData()
  },
  onShareAppMessage() {},
  /**
   * 获取首页数据
   */
  getHomeData() {
    wx.showLoading({ title: '拼命加载中' })
    wxRequest.get({
      url: '/api/Index/index',
      callback: wx.hideLoading
    })
      .then(res => {
        console.log(res.code === API_OK)
        if (res.code === API_OK) {
          this.setData({
            loading: false,
            banner: res.data.data.map(item => {
              return {
                id: item.id,
                // mode: item.position === 1 ? 'right-padding-30' : item.position === 2 ? 'left-padding-30' : '',
                image: item.images,
                title: item.title,
                sub: item.en_title,
                describe: item.desc,
                link: item.page
              }
            })
          })
        }
      })
      .catch(err => {
        console.warn(err)
        this.setData({ loading: false })
      })
  },
})
