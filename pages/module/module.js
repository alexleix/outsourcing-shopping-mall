const { activityListApi } = require('../../api/module')
const { courseList } = require('../../api/course')
const { goodsList } = require('../../api/goods')
const { API_OK } = require('../../utils/setting')
const app =  getApp()

Page({
  data: {
    headerBtnType: 'circle',
    headerBgColor: 'transparent',
    loading: false,
    headerImage: '',
    list: [],
    type: '',
    title: '',
    sub: '',
    buttonText: ''
  },
  pageData: {
    listApi: null
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    console.log(options)
    wx.setNavigationBarColor({ frontColor: '#ffffff', backgroundColor: '#000000' })
    this.setPageData(options.type)
    this.setData({ type: options.type, loading: true, headerImage: options.image })
  },
  onReady() {
    wx.showLoading({ title: '拼命加载中', mask: true })
    this.getListData()
  },
  onPullDownRefresh() {
    wx.showLoading({ title: '拼命加载中' })
    this.getListData()
  },
  /**
   * 跳转分类页
   */
  goCategoryPage() {
    wx.navigateTo({ url: `/pages/category/category?type=${this.data.type}` })
  },

  /**
   * 根据类型设置页面内容
   * @param {string} type 类型
   */
  setPageData(type) {
    switch(type) {
      case 'hot':
        this.pageData.listApi = this.getGoods
        this.setData({ title: '经典热卖', sub: '件商品', buttonText: '查看全部商品' })
        break
      case 'course':
        this.pageData.listApi = this.getExcellentCourse
        this.setData({ title: '星球分享', sub: '个星球', buttonText: '查看全部星球' })
        break
      case 'activity':
        this.pageData.listApi = this.getActivityList
        this.setData({ title: '最新活动', sub: '个活动', buttonText: '查看全部活动' })
        break
      case 'credit':
        this.pageData.listApi = this.getGoods
        this.setData({ title: '积分兑换', sub: '件商品', buttonText: '查看全部商品' })
        break
      default:
        this.pageData.listApi = this.getGoods
        this.setData({ title: '灵感周边', sub: '件商品', buttonText: '查看全部商品' })
    }
  },
  /**
   * 获取列表数据
   */
  getListData() {
    this.pageData.listApi && this.pageData.listApi()
  },

  /**
   * 获取热卖商品列表
   */
  async getGoods() {
    try {
      const res = await goodsList({
        data: {
          type: this.data.type === 'hot' ? 1 : this.data.type === 'credit' ? 2 : 3,
          category_id: 0,
          keywords: '',
          is_total: 0,
          is_sales: this.data.type === 'hot' ? 1 : 0,
          is_new: 0,
          is_price: 0,
          p: 1,
          paginate: 16
        },
        callback: () => {
          wx.hideLoading()
          wx.stopPullDownRefresh()
        }
      })
      // console.log(res)
      if (res.code === API_OK) {
        this.setData({
          loading: false,
          list: res.data.list.data.map(goods => {
            return {
              id: goods.goods_id,
              image: (goods.image && goods.image.length) ? goods.image[0].file_path : '',
              name: goods.goods_name,
              price: goods.goods_price,
              credit: this.data.type === 'credit' ? goods.score_deduct * 1 : undefined
            }
          })
        })
      }
    } catch(err) {
      console.warn(err)
    }
  },

  /**
   * 获取活动列表
   */
  getActivityList() {
    activityListApi({
      data: { paginate: 15, page: this.pageData.page },
      callback: () => {
        wx.hideLoading()
        wx.stopPullDownRefresh()
      }
    })
      .then(res => {
        if (res.code === API_OK) {
          // console.log(res.data)
          this.setData({
            loading: false,
            list: res.data.data.map(item => {
              return {
                id: item.id,
                image: item.images.length && item.images[0].file_path,
                name: item.title,
                address: item.address,
                time: item.start_end_time,
                likeNumber: item.likes_count
              }
            })
          })
        }
      })
      .catch(err => {
        console.warn(err)
      })
  },
  /**
   * 获取精品课程
   */
  getExcellentCourse() {
    courseList({
      data: { category_id: 0, sortType: 'sales', sortPrice: 1, search: '', page: 1, paginate: 15 },
      callback: () => {
        wx.hideLoading()
        wx.stopPullDownRefresh()
      }
    })
      .then(res => {
        if (res.code === API_OK) {
          this.setData({
            loading: false,
            list: res.data.data.map(item => {
              return {
                id: item.courses_id,
                image: (item.image && item.image.length) && item.image[0].file_path,
                courseName: item.course_name,
                classHour: item.lessons_count,
                purchasedNum: item.sold_count,
                coursePrice: item.course_price
              }
            })
          })
        }
      })
      .catch(err => {
        console.warn(err)
      })
  },

  onPageScroll({scrollTop}) {
    let btnType, bgColor

    if (scrollTop >= (app.globalData.viewportWidth * 0.75 - app.globalData.headerHeight)) {
      btnType = 'common'
      bgColor = '#fff'
    } else {
      btnType = 'circle'
      bgColor = 'transparent'
    }
    if (btnType !== this.data.headerBtnType) {
      if (btnType === 'circle') {
        wx.setNavigationBarColor({ frontColor: '#ffffff', backgroundColor: '#000000' })
      } else {
        wx.setNavigationBarColor({ frontColor: '#000000', backgroundColor: '#000000' })
      }
      this.setData({ headerBtnType: btnType, headerBgColor: bgColor })
    }
  },
})
