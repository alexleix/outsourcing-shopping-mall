const { goodsList } = require('../../api/goods')
const { API_OK } = require('../../utils/setting')

Page({
  data: {
    banner: [],
    index: 0,
    describe: '吉他在流行音乐、摇滚音乐、蓝调、民歌、佛朗明哥中，常被视为主要乐器。而在古典音乐的领域里，吉他常以独奏或二重奏的型式演出。'
  },
  onLoad: function(){
    wx.showLoading({ title: '拼命加载中', mask: true })
    this.getBannerData()
  },
  /**
   * 轮播切换
   */
  handleSwiperChange({ detail }) {
    this.setData({ index: detail.current })
  },
  /**
   * 获取商品信息
   */
  async getBannerData() {
    try {
      const res = await goodsList({
        data: {
          type: 1,
          is_new: 1,
          p: 1,
          per_page: 5
        },
        callback: wx.hideLoading
      })
      // console.log(res)
      if (res.code === API_OK) {
        this.setData({
          banner: res.data.list.data.map(item => {
            return {
              id: item.goods_id,
              name: item.goods_name,
              describe: item.goods_jianjie,
              image: item.image && item.image.length && item.image[0].file_path
            }
          })
        })
      }
    } catch(err) {
      console.warn(err)
    }
  }
})
